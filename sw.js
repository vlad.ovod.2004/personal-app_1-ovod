const staticCacheName = 'site-static-v1';
const assets = [
  '/',
  '/index.html',
  '/script/script.js',
  '/styles/style.css',
  '/images/Piano.png',
  '/images/arrow_back.png',
  '/images/arrow_forth.png',
  '/sound/1Scale.mp3',
  '/sound/2Scale.mp3',
  '/sound/3Scale.mp3',
  '/sound/4Scale.mp3',
  '/sound/5Scale.mp3',
  '/sound/6Scale.mp3',
  '/sound/7Scale.mp3',
  '/sound/8Scale.mp3',
  'https://fonts.googleapis.com/css2?family=Lexend+Tera&display=swap',
];
// install event
self.addEventListener('install', evt => {
  evt.waitUntil(
    caches.open(staticCacheName).then((cache) => {
      console.log('caching shell assets');
      cache.addAll(assets);
    })
  );
});
// activate event
self.addEventListener('activate', evt => {
  evt.waitUntil(
    caches.keys().then(keys => {
      return Promise.all(keys
        .filter(key => key !== staticCacheName)
        .map(key => caches.delete(key))
      );
    })
  );
});
// fetch event
self.addEventListener('fetch', evt => {
  evt.respondWith(
    caches.match(evt.request).then(cacheRes => {
      return cacheRes || fetch(evt.request);
    })
  );
});