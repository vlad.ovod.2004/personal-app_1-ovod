var visualiser, numOfBars, array, context, bars, analyser, src, height;

visualiser = document.getElementById('visualiser');

numOfBars = 16;

array = new Uint8Array(numOfBars*4);

document.querySelector('.text').onclick = function(){
    if (context) return;

    document.querySelector('.text').textContent = 'Chromatic scale';
    bars = document.getElementsByClassName('bar');
    context = new AudioContext();
    analyser = context.createAnalyser();

    navigator.mediaDevices.getUserMedia({
        audio: true
    }).then(stream => {
        src = context.createMediaStreamSource(stream);
        src.connect(analyser);
        loop();
    }).catch(error => {
        alert(error);
        location.reload();
    });

}
function loop() {
    window.requestAnimationFrame(loop);
    document.querySelector('.visualiser').style.display = 'flex';
    analyser.getByteFrequencyData(array);
    for(var i = 0 ; i < numOfBars; i++){
        height = array[i * 4] / 3;
        bars[i].style.minHeight = height+'px';
    }
    const volume = array.reduce((acc, cur) => acc + cur)/array.length;
    if (volume > 110) {
        const audio = new Audio();
        audio.src = 'sound/8Scale.mp3'
        audio.play();
    }
    else if (volume > 105) {
        const audio = new Audio();
        audio.src = 'sound/7Scale.mp3'
        audio.play();
    }
    else if (volume > 100) {
        const audio = new Audio();
        audio.src = 'sound/6Scale.mp3'
        audio.play();
    }
    else if (volume > 95) {
        const audio = new Audio();
        audio.src = 'sound/5Scale.mp3'
        audio.play();
    }
    else if (volume > 90) {
        const audio = new Audio();
        audio.src = 'sound/4Scale.mp3'
        audio.play();
    }
    else if (volume > 85) {
        const audio = new Audio();
        audio.src = 'sound/3Scale.mp3'
        audio.play();
    }
    else if (volume > 80) {
        const audio = new Audio();
        audio.src = 'sound/2Scale.mp3'
        audio.play();
    }
    else if (volume > 75) {
        const audio = new Audio();
        audio.src = 'sound/1Scale.mp3'
        audio.play();
    }
}